extern crate rand;
use rand::Rng;
use std::io;
use std::cmp::Ordering;

fn main() {
    println!("Hello gamer!");

    let min_num = 0;
    let max_num = 10;
    let secret = rand::thread_rng().gen_range(min_num, max_num + 1);
    // println!("The secret is: {} ... oops", secret);

    loop {
        println!("Please guess a number between {} and {}:", min_num, max_num);
        let mut guess = String::new();
        io::stdin().read_line(&mut guess).expect(" Read failed");
        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        println!("Your guess was: {}", guess);

        match guess.cmp(&secret) {
            Ordering::Less => println!(" Too small"),
            Ordering::Greater => println!(" Too big"),
            Ordering::Equal => {
                println!(" Correct");
                break;
            },
        }
    }
}
